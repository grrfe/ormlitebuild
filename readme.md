# ormlite-build

This repository provides [ormlite-core](https://github.com/j256/ormlite-core) 
and [ormlite-jdbc](https://github.com/j256/ormlite-jdbc) package in one repository 
since Java 9+'s modulesystem can't handle the same package(s) from multiple dependencies 

**Usable via**

[![](https://jitpack.io/v/com.gitlab.grrfe/ormlitebuild.svg)](https://jitpack.io/#com.gitlab.grrfe/ormlitebuild)
