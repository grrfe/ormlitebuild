plugins {
    java
    `maven-publish`
    id("net.nemerosa.versioning")
}

group = "com.j256.ormlite"
version = versioning.info.tag ?: versioning.info.full

repositories {
    mavenCentral()
}

java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(21)
    }
}

dependencies {
    implementation(libs.persistence.api)
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = project.group.toString()
            version = project.version.toString()

            from(components["java"])
        }
    }
}
