if [ ! -d "dependency" ]
then
  git clone https://github.com/j256/ormlite-core dependency/core/
  git clone https://github.com/j256/ormlite-jdbc dependency/jdbc/
fi

cd dependency/core
git pull
cd ../jdbc
git pull

cd ../..

cp -r dependency/core/src/main/ src/
cp -r dependency/jdbc/src/main/ src/

