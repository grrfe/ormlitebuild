package com.j256.ormlite.logger;

import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;

import java.io.Serializable;

public class Logger {
    public void debug(String s, String url, int i) {
    }

    public void debug(String s, int i) {
    }

    public boolean isLevelEnabled(Level level) {
        return false;
    }

    public void trace(String s, Object... args) {
    }

    public void error(String s, Object... args) {
    }


    public void debug(String s, String label, Object... args) {
    }

    public void error(String s, String label, Object... args) {
    }

    public void error(Exception e, String s) {
    }

    public void warn(String s, ClassNotFoundException e) {
    }

    public void debug(String closing) {
    }

    public void debug(String s, Object... args) {
    }

    public void debug(Exception e, String s, Object... args) {
    }

    public void warn(Throwable th, String s, Object... args) {
    }

    public void info(String s, Object... args) {
    }

    public void log(Level logLevel, Exception e, Object... args) {
    }
}
