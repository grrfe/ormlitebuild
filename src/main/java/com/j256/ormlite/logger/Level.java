package com.j256.ormlite.logger;

public enum Level {
    DEBUG, TRACE
}
